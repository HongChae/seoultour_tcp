import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;


public class ThreadServer implements Runnable {
	
	private Socket mClientSocket;

	public ThreadServer(Socket clientSocket) {
		mClientSocket = clientSocket;
	}

	public void run() {
		try {
			while (true) {

				InputStream inputStream = mClientSocket.getInputStream();
				OutputStream outputStream = mClientSocket.getOutputStream();
				Date today = new Date();

				int readCount = 0;
				byte[] readBuffer = new byte[4096];

				readCount = inputStream.read(readBuffer);
				String fileName = new String(readBuffer, 0, readCount);

				System.out.println("Client 가 요청한 파일 : " + fileName);
				File file = new File("C:/seoultour/", fileName);

				FileInputStream fileInputStream = new FileInputStream(file);

				byte[] buffer = new byte[4096];

				while (true) {
					int fileLength = fileInputStream.read(buffer);
					if (fileLength == -1) {
						break;
					} else {
						outputStream.write(buffer, 0, fileLength);
					}
				}
				fileInputStream.close();

				System.out.println("파일보내기 완료");

				int read = 0;
				byte[] readbuf = new byte[4096];

				read = inputStream.read(readbuf);
				String fileName2 = new String(readbuf, 0, read);

				System.out.println("Client 가 요청한 파일  : " + fileName2);
				File file2 = new File("C:/seoultour/", fileName2);

				FileInputStream fin = new FileInputStream(file2);

				byte[] buffer2 = new byte[4096];

				int count = 0;
				while (true) {
					int fileLength = fin.read(buffer2);
					if (fileLength == -1) {
						break;
					} else {
						System.out.println("read write count : " + ++count);
						outputStream.write(buffer2, 0, fileLength);
					}
				}
				outputStream.flush();
				System.out.println("db 파일보내기 완료 " + today);
				fin.close();
				inputStream.close();
				outputStream.close();
				mClientSocket.close();				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}