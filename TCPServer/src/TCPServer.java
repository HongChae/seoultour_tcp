import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
	ServerSocket serverSocket;

	public static void main(String[] args) throws IOException {		
		ServerSocket serverSocket = new ServerSocket(5555);		
		
		while(true) {
			Socket clientSocket = serverSocket.accept(); 
			ThreadServer  threadServer = new ThreadServer(clientSocket);
			Thread t = new Thread(threadServer);
			t.start();
		}
	}
}